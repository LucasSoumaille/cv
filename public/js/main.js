window.onresize = resize;
var generateSkillsBool = false;
var generateExpProBool = false;
var generateProjects = false;

/* Test with cookie for pers project pages
document.cookie = "reloaded=0; expires=Thu, 01 Jan 1970 00:00:00 UTC"; 
function setCookie(cname, cvalue, exdays) {
  const d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  let expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  let name = cname + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i <ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}*/

function resize() {
  width =  window.innerWidth;
  if(width < 1500) {
    document.getElementById("profil-header-img").classList.remove("col-md-3");
    document.getElementById("profil-header-text").classList.remove("col-md-9");
    document.getElementById("profil-header-img").classList.add("col-md-12");
    document.getElementById("profil-header-text").classList.add("col-md-12")
  } else {
    document.getElementById("profil-header-img").classList.add("col-md-3");
    document.getElementById("profil-header-text").classList.add("col-md-9");
    document.getElementById("profil-header-img").classList.remove("col-md-12");
    document.getElementById("profil-header-text").classList.remove("col-md-12");
  }

  if(width < 1000) {
    document.getElementById("chart1").classList.remove("col-md-5");
    document.getElementById("chart1").classList.add("col-md-12");
    document.getElementById("chart2").classList.remove("col-md-5");
    document.getElementById("chart2").classList.add("col-md-12");
    document.getElementById("left-skills").classList.remove("col-md-6");
    document.getElementById("left-skills").classList.add("col-md-12");
    document.getElementById("right-skills").classList.remove("col-md-6");
    document.getElementById("right-skills").classList.add("col-md-12");
  } else {
    document.getElementById("chart1").classList.add("col-md-5");
    document.getElementById("chart1").classList.remove("col-md-12");
    document.getElementById("chart2").classList.add("col-md-5");
    document.getElementById("chart2").classList.remove("col-md-12");
    document.getElementById("left-skills").classList.add("col-md-6");
    document.getElementById("left-skills").classList.remove("col-md-12");
    document.getElementById("right-skills").classList.add("col-md-6");
    document.getElementById("right-skills").classList.remove("col-md-12");
  }
}


function changeContent(value) {
  document.getElementById("sidebar").classList.remove("active");
  document.getElementById("sidebarCollapse").classList.remove("active");
  if (value == "parcours") {
    document.getElementById("nameOnglet").innerHTML = "Parcours";
    document.getElementById("Profile").className = "slideOut"
    document.getElementById("LineTime").className = "slideIn";
    document.getElementById("skills").className = "slideOut";
    document.getElementById("expPro").className = "slideOut";
    document.getElementById("projects").className = "slideOut";
  } else if (value == "profile") {
    document.getElementById("nameOnglet").innerHTML = "Profil";
    document.getElementById("LineTime").className = "slideOut"
    document.getElementById("Profile").className = "slideIn";
    document.getElementById("skills").className = "slideOut"
    document.getElementById("expPro").className = "slideOut";
    document.getElementById("projects").className = "slideOut";
  } else if (value == "skills") {
    document.getElementById("nameOnglet").innerHTML = "Compétences";
    document.getElementById("Profile").className = "slideOut"
    document.getElementById("LineTime").className = "slideOut"
    document.getElementById("skills").className = "slideIn"
    document.getElementById("expPro").className = "slideOut";
    document.getElementById("projects").className = "slideOut";
    if(!(generateSkillsBool)) {
      drawSkills();
      generateSkills();
      generateSkillsBool = true;
    }
  } else if (value == "exppro") {
    document.getElementById("nameOnglet").innerHTML = "Expérience professionnelle";
    document.getElementById("Profile").className = "slideOut"
    document.getElementById("LineTime").className = "slideOut"
    document.getElementById("expPro").className = "slideIn";
    document.getElementById("skills").className = "slideOut";
    document.getElementById("projects").className = "slideOut";
    if(!(generateExpProBool)) {
      generateExperiences();
      generateExpProBool = true;
    }
  } else if (value == "projets") {
    document.getElementById("nameOnglet").innerHTML = "Projets personnels";
    document.getElementById("Profile").className = "slideOut"
    document.getElementById("LineTime").className = "slideOut"
    //document.getElementById("projects").className = "slideIn";
    document.getElementById("expPro").className = "slideOut";
    document.getElementById("skills").className = "slideOut";
    document.getElementById("projects").className = "slideIn";
    if(!(generateExpProBool)) {
      generatePersProjects();
      generateProjects = true;
    }
    setTimeout(function(){
      document.getElementById("projects").className = "slideIn";
    }, 100);
  }
  this.animeNameOnglet();
}

function initFunction() {
  /*if(getCookie("reloaded") == "0"){
    console.log("pas rechargé, recharge");
    console.log(getCookie("reloaded"));
    setCookie("reloaded", "1", 7);
    document.cookie = "reloaded=1; expires=Thu, 01 Jan 2023 00:00:00 UTC"; 
    
    setTimeout(function(){
      document.getElementById("projects").className = "slideIn";
      console.log(getCookie("reloaded"));
    }, 1000);
  }
  //document.location.reload(true);*/
    
  const isMobile = matchMedia('(max-device-width: 20em)').matches;
  resize();
  document.getElementById("nameOnglet").innerHTML = "Profil";
  this.animeNameOnglet();
  // création Timeline
  var elementHTML = document.getElementsByClassName("timeline")[0];
  var items = experiences.elements;
  for(var i = 0; i < items.length; i++) {
    var li = document.createElement("li");
    li.classList.add("timeline-item");
    var divInfo = document.createElement("div");
    divInfo.classList.add("timeline-info");
    var span = document.createElement("span");
    span.innerHTML = items[i].date;
    divInfo.appendChild(span);
    li.appendChild(divInfo);
    var divMarker = document.createElement("div");
    divMarker.classList.add("timeline-marker");
    li.appendChild(divMarker);
    var divContent = document.createElement("div");
    divContent.classList.add("timeline-content");
    var h3 = document.createElement("h3");
    var p = document.createElement("p");
    h3.innerHTML = items[i].title;
    h3.classList.add("timeline-title")
    h3.classList.add("title")
    p.innerHTML = items[i].content;
    divContent.appendChild(h3);
    divContent.appendChild(p);
    li.appendChild(divContent);
    elementHTML.appendChild(li);
  }
  // création profil
  var elementHTML2 = document.getElementById("Profile");
  var itemsProfiles = profiles.elements;
  for(var i = 0; i < itemsProfiles.length; i++) {
    var div = document.createElement("div");
    div.classList.add("a-propos");
    var title = document.createElement("h3");
    title.classList.add("title-profile")
    title.innerHTML = itemsProfiles[i].title;
    div.appendChild(title);
    var content = document.createElement("p")
    content.classList.add("text-profil");
    content.innerHTML = itemsProfiles[i].content;
    div.appendChild(content);
    elementHTML2.appendChild(div);
    if(i+1 != itemsProfiles.length) {
      var divider = document.createElement("div");
      divider.classList.add("divider")
      divider.classList.add("div-transparent")
      elementHTML2.appendChild(divider);
    }
  }
}

function drawSkills() {
  var ctx = document.getElementById('chartSkills1').getContext('2d');
  var ctx2 = document.getElementById('chartSkills2').getContext('2d');
  Chart.defaults.elements.point.radius = 5;
  Chart.defaults.elements.point.hoverRadius = 10;
  var myChart = new Chart(ctx, {
    type: 'radar',
    data: dataChart1,
    options: {
      responsive : true,
      maintainAspectRatio : true,
      plugins: {
        title: {
          display: true,
          text: 'Diagramme de compétences professionnelles',
          color : "white",
          font : {size : 18},
          padding: {
            top: 30,
            bottom: 30
          }
        },
        legend : {
          display : true,
          position : 'bottom',
          labels : {
            color : 'white'
          }
        }
      },
      scales: {
        r: {
          angleLines: {
            color: 'rgb(255,255,255,1)'
          },
          grid: {
            color: ['rgb(255,255,255,0.2)',
            'rgb(255,255,255,0.2)',
            'rgb(255,255,255,0.2)',
            'rgb(255,255,255,0.2)',
            'rgb(255,255,255,0.2)',
            'rgb(255,255,255,0.2)',
            'rgb(255,255,255,0.2)',
            'rgb(255,255,255,0.2)',
            'rgb(255,255,255,0.2)',
            'rgb(255,255,255,1)']

          },
          pointLabels: {
            font : {size : 15},
            color: 'white',
            backdropColor : "rgb(255,255,255,0)"
          },
          ticks: {
            font : {size : 13},
            color: 'white',
            backdropColor : "rgb(255,255,255,0)"
          }
        }
      },
      scale: {
        beginAtZero: true,
        max: 100,
        min: 0,
        stepSize: 10,
      },
    }
  });
  var myChart2 = new Chart(ctx2, {
    type: 'radar',
    data: dataChart2,
    options: {
      responsive : true,
      maintainAspectRatio : true,
      plugins: {
        title: {
          display: true,
          text: 'Traits de caractère',
          color : "white",
          font : {size : 18},
          padding: {
            top: 30,
            bottom: 30
          }
        },
        legend : {
          display : true,
          position : 'bottom',
          labels : {
            color : 'white'
          }
        }
      },
      scales: {
        r: {
          angleLines: {
            color: 'rgb(255,255,255,1)'
          },
          grid: {
            color: ['rgb(255,255,255,0.2)',
            'rgb(255,255,255,0.2)',
            'rgb(255,255,255,0.2)',
            'rgb(255,255,255,0.2)',
            'rgb(255,255,255,0.2)',
            'rgb(255,255,255,0.2)',
            'rgb(255,255,255,0.2)',
            'rgb(255,255,255,0.2)',
            'rgb(255,255,255,0.2)',
            'rgb(255,255,255,1)']

          },
          pointLabels: {
            font : {size : 15},
            color: 'white',
            backdropColor : "rgb(255,255,255,0)"
          },
          ticks: {
            font : {size : 13},
            color: 'white',
            backdropColor : "rgb(255,255,255,0)"
          }
        }
      },
      scale: {
        beginAtZero: true,
        max: 100,
        min: 0,
        stepSize: 10,
      },
    }
  });
}

function generateSkills(){
  var containerSkills1 = document.getElementById("left-skills");
  var containerSkills2 = document.getElementById("right-skills");
  var count = 0;
  for(var i = 0; i < skills.topics.length; i++) {
    for(var j = 0; j < skills.topics[i].skills.length; j++) {
      count += 1;
    }
  }
  var currentItem = 1 ;
  var broke = false;
  for(var i = 0; i < skills.topics.length; i++) {
    var h3MainTitleSkill = document.createElement("h4");
    h3MainTitleSkill.innerHTML = skills.topics[i].title;
    h3MainTitleSkill.classList.add("title");
    if(!broke) {
      containerSkills1.appendChild(h3MainTitleSkill);
    } else {
      containerSkills2.appendChild(h3MainTitleSkill);
    }
    for(var j = 0; j < skills.topics[i].skills.length; j++) {
      var divResumeProTop = document.createElement("div");
      divResumeProTop.innerHTML = skills.topics[i].skills[j].label;
      divResumeProTop.classList.add("resumeProficienciesTop");
      var divItemSkills = document.createElement("div");
      var divResumeProBottom = document.createElement("div");
      var divProgress = document.createElement("div");
      var divRapidproto = document.createElement("div");
      var spanProgressBar = document.createElement("span");
      divItemSkills.classList.add("itemSkill");
      divResumeProBottom.classList.add("resumeProficienciesBottom");
      divProgress.classList.add("progress");
      divRapidproto.classList.add("rapidproto");
      divRapidproto.classList.add("progress-bar");
      divRapidproto.classList.add("progress-bar-info");
      divRapidproto.setAttribute("role", "progressbar");
      var valueBar = skills.topics[i].skills[j].value;
      divRapidproto.style.width = String(valueBar) +"%";
      if(valueBar < 25) {
        divRapidproto.classList.add("orange");
      } else if (valueBar >= 25 && valueBar < 50) {
        divRapidproto.classList.add("yellow");
      } else if (valueBar >= 50 && valueBar < 75) {
        divRapidproto.classList.add("blue");
      } else {
        divRapidproto.classList.add("green");
      }
      spanProgressBar.classList.add("sr-only");
      divItemSkills.appendChild(divResumeProTop);
      divRapidproto.appendChild(spanProgressBar);
      divProgress.appendChild(divRapidproto);
      divResumeProBottom.appendChild(divProgress)
      divItemSkills.appendChild(divResumeProBottom);
      if(!broke) {
        containerSkills1.appendChild(divItemSkills);
      } else {
        containerSkills2.appendChild(divItemSkills);
      }
      currentItem += 1;
      if (currentItem > count/2) broke=true;
    }
  }
}

function animeNameOnglet() {
  var textWrapper = document.querySelector('.ml10 .letters');
  textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

  anime.timeline({loop: false})
  .add({
    targets: '.ml10 .letter',
    rotateY: [-90, 0],
    duration: 1300,
    delay: (el, i) => 45 * i
  });
}

function generateExperiences() {
  var expProContainer = document.getElementById("expPro");
  for (var i = 0; i < expPro.experiences.length; i++) {
    var divRow = document.createElement("div");
    var divExpro = document.createElement("div");
    var divTopExpro = document.createElement("div");
    var h3Exppro = document.createElement("h3");
    var iExppro = document.createElement("i");
    var pExpproDate = document.createElement("p");
    var pExpproRole = document.createElement("p");
    var pExpproContent = document.createElement("p");
    divTopExpro.classList.add("top-expro");
    divRow.classList.add("row");
    divRow.classList.add("row-expPro");
    iExppro.classList.add("fas");
    pExpproDate.classList.add("dateExppro");
    pExpproRole.classList.add("roleExpro");
    pExpproContent.classList.add("contentExppro");
    iExppro.classList.add(expPro.experiences[i].img);
    h3Exppro.innerHTML = expPro.experiences[i].title;
    pExpproDate.innerHTML = expPro.experiences[i].date;
    pExpproRole.innerHTML = expPro.experiences[i].role;
    pExpproContent.innerHTML = expPro.experiences[i].content;
    divTopExpro.appendChild(h3Exppro);
    divTopExpro.appendChild(iExppro);
    divExpro.classList.add("col-md-5");
    divExpro.appendChild(divTopExpro);
    divExpro.appendChild(pExpproDate);
    divExpro.appendChild(pExpproRole);
    divExpro.appendChild(pExpproContent);
    divExpro.classList.add("container-expPro");
    if(i%2 == 0) {
      expProContainer.appendChild(divRow);
    }
    var divRowExist = document.getElementsByClassName("row-expPro")[Math.floor(i/2)];
    divRowExist.appendChild(divExpro);
  }
}

function generatePersProjects() {
  var projectPers = document.getElementById("projects");
  for (var i = 0; i < project.projets.length; i++) {
    var divItemProjet = document.createElement("div");
    divItemProjet.classList.add("itemProjet");
    var titleProjet = document.createElement("h3");
    titleProjet.innerHTML = project.projets[i].title;
    var divcarousel = document.createElement("div");
    divcarousel.classList.add("carousel");
    divcarousel.classList.add("slide");
    divcarousel.setAttribute('id', project.projets[i].idCarousel);
    divcarousel.setAttribute('data-interval', "false");
    var olCarousel = document.createElement("ol");
    olCarousel.classList.add("carousel-indicators");
    var divCarouselInner = document.createElement("div");
    divCarouselInner.classList.add("carousel-inner")
    if (project.projets[i].images.length != 0) {
      for (var k = 0; k < project.projets[i].images.length; k++) {
        var liCarIndic = document.createElement("li");
        liCarIndic.setAttribute('data-target', "#" + project.projets[i].idCarousel);
        liCarIndic.setAttribute('data-slide-to', k);
        var divCarItem = document.createElement("div");
        divCarItem.classList.add("carousel-item");
        var imgCarItem = document.createElement("img");
        imgCarItem.classList.add("d-block");
        imgCarItem.classList.add("w-100");
        imgCarItem.setAttribute('src', project.projets[i].images[k].link);
        imgCarItem.setAttribute('alt', k);
        var divCarCaption = document.createElement("div");
        divCarCaption.classList.add("carousel-caption");
        divCarCaption.classList.add("d-none");
        divCarCaption.classList.add("d-md-block");
        if (project.projets[i].images[k].legend != "") {
          var h5CarCaption = document.createElement("h5");
          h5CarCaption.innerHTML = project.projets[i].images[k].legend;
          divCarCaption.appendChild(h5CarCaption);
        }
        if (k == 0) {
          divCarItem.classList.add("active");
          liCarIndic.setAttribute('class', "active");
        }
        divCarItem.appendChild(imgCarItem);
        divCarItem.appendChild(divCarCaption);
        divCarouselInner.appendChild(divCarItem);
        olCarousel.appendChild(liCarIndic);
      }
      if (project.projets[i].images.length != 1) {
        var aCarouselPrev = document.createElement("a");
        var aCarouselNext = document.createElement("a");
        aCarouselPrev.classList.add("carousel-control-prev")
        aCarouselPrev.setAttribute('href', "#" + project.projets[i].idCarousel);
        aCarouselPrev.setAttribute('role', "button");
        aCarouselPrev.setAttribute('data-slide', "prev");
        var spanCarouselPrev1 = document.createElement("i");
        var spanCarouselPrev2 = document.createElement("span");
        spanCarouselPrev1.setAttribute('aria-hidden', "true");
        spanCarouselPrev1.classList.add("fas");
        spanCarouselPrev1.classList.add("fa-chevron-left");
        spanCarouselPrev2.classList.add("sr-only");
        spanCarouselPrev2.innerHTML = "Previous";
        aCarouselPrev.appendChild(spanCarouselPrev1);
        aCarouselPrev.appendChild(spanCarouselPrev2);
        aCarouselNext.classList.add("carousel-control-next")
        aCarouselNext.setAttribute('href', "#" + project.projets[i].idCarousel);
        aCarouselNext.setAttribute('role', "button");
        aCarouselNext.setAttribute('data-slide', "next");
        var spanCarouselNext1 = document.createElement("i");
        var spanCarouselNext2 = document.createElement("span");
        spanCarouselNext1.setAttribute('aria-hidden', "true");
        spanCarouselNext1.classList.add("fas");
        spanCarouselNext1.classList.add("fa-chevron-right");
        spanCarouselNext2.classList.add("sr-only");
        spanCarouselNext2.innerHTML = "Next";
        aCarouselNext.appendChild(spanCarouselNext1);
        aCarouselNext.appendChild(spanCarouselNext2);
        divcarousel.appendChild(olCarousel);
        divcarousel.appendChild(divCarouselInner);
        divcarousel.appendChild(aCarouselPrev);
        divcarousel.appendChild(aCarouselNext);
      } else {
        divcarousel.appendChild(olCarousel);
        divcarousel.appendChild(divCarouselInner);
      }
      } else {
      var pNoImage = document.createElement("p");
      pNoImage.classList.add("profil-image-div");
      pNoImage.innerHTML = "Pas d'image pour ce projet";
      divcarousel.appendChild(pNoImage);
      divcarousel.appendChild(olCarousel);
      divcarousel.appendChild(divCarouselInner);
    }
    var divgitProjetPers = document.createElement("div");
    divgitProjetPers.classList.add("gitProjetPers");
    var divgitgitlab = document.createElement("div");
    var spangitlab = document.createElement("span");
    var gitContent = document.createElement("h5");
    divgitgitlab.classList.add("gitlab");
    spangitlab.classList.add("fab");
    spangitlab.classList.add("fa-gitlab");
    divgitgitlab.appendChild(spangitlab);
    gitContent.innerHTML = project.projets[i].git;
    var divcalendarProjetPers = document.createElement("div");
    divcalendarProjetPers.classList.add("calendarProjetPers");
    var iconCalendar = document.createElement("i");
    iconCalendar.classList.add("far");
    iconCalendar.classList.add("fa-calendar-alt");
    var contentCalendar = document.createElement("h5");
    contentCalendar.innerHTML = "Période : " + project.projets[i].periode;
    var divtechnoProjetPers = document.createElement("div");
    divtechnoProjetPers.classList.add("technoProjetPers");
    var iconTechno = document.createElement("i");
    iconTechno.classList.add("fas");
    iconTechno.classList.add("fa-file-code");
    var contentTechno = document.createElement("h5");
    var technosList = "Technologies : ";
    for (var j=0; j<project.projets[i].technos.length; j++) {
      technosList += project.projets[i].technos[j];
      if(j != project.projets[i].technos.length - 1) {
        technosList += ", "
      }
    }
    contentTechno.innerHTML = technosList;
    var pContentDesc = document.createElement("p");
    pContentDesc.innerHTML = project.projets[i].content;
    divgitProjetPers.appendChild(divgitgitlab);
    divgitProjetPers.appendChild(gitContent);
    divcalendarProjetPers.appendChild(iconCalendar);
    divcalendarProjetPers.appendChild(contentCalendar);
    divtechnoProjetPers.appendChild(iconTechno);
    divtechnoProjetPers.appendChild(contentTechno);
    divItemProjet.appendChild(titleProjet)
    divItemProjet.appendChild(divcarousel)
    divItemProjet.appendChild(divgitProjetPers)
    divItemProjet.appendChild(divcalendarProjetPers)
    divItemProjet.appendChild(divtechnoProjetPers)
    divItemProjet.appendChild(pContentDesc)
    projectPers.appendChild(divItemProjet);
    if(i+1 != project.projets.length) {
      var divider = document.createElement("div");
      divider.classList.add("divider")
      divider.classList.add("div-transparent")
      projectPers.appendChild(divider);
    }
  }
}
