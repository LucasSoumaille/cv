const experiences = {
  "elements": [
    {
      "date": "2025",
      "title": "Thales Defense Mission Systems - Bordeaux",
      "content": "<b>Changement d'entreprise</b>\n\nEn 2025, je suis muté pour rejoindre Thales Defense Mission Systems toujours avec un rôle d'architecte applicatif et TechLead"
    },
    {
      "date": "2023",
      "title": "Thales Services Numériques - Bordeaux",
      "content": "<b>Changement d'entreprise</b>\n\nEn 2023, je change d'entreprise pour rejoindre Thales Services Numériques, société de service en informatique et je travail pour le groupe Thales sur des projets avec un rôle de TechLead - Architecte applicatif"
    },
    {
      "date": "2020",
      "title": "EPSI - CGI - Bordeaux",
      "content": "<b>Bac +5 : Expertise informatique et système d'information</b>\n\nSuite à l'obtention de mon Bac +3 en développement, j'ai voulu continuer en Bac +5 afin d'approfondir mes connaissances en informatique, en développement, architecture logicielle, management, etc.\n\nCe souhait a été entendu par ma hiérarchie, j'ai pu continuer mon alternance pour 2 ans supplémentaires chez CGI.\n\nCes deux dernières années, obtenues avec les félicitations du jury, m'ont permis d'acquérir une vue d'ensemble de plusieurs technologies et méthodologies (projets, systèmes d'exploitation, languages, bases de données, etc.).\n2020 marque la fin de mes études et mon embauche en CDI chez CGI."
    },
    {
      "date": "2018",
      "title": "EPSI - CGI - Bordeaux",
      "content": "<b>Bac +3 : Concepteur développeur, Programmation informatique</b>\n\nAprès l'obtention de ma Licence de Sociologie, j'ai souhaité me réorienter dans le milieu de l'informatique comme cela a toujours été mon souhait.\n\nCela a pris un peu plus de 2 ans et demi, mais à force de déterminiation et avec la formation U'Dev ouverte chez CGI, j'ai pu réaliser ce souhait.\n\nCette formation permet à des Bac +2 et plus d'obtenir un Bac +3 en 1 an, au cours d'une formation intensive alliant apports de connaissances théoriques à l'école et alternance avec CGI."
    },
    {
      "date": "2014",
      "title": "Université Victor Segalen - Bordeaux",
      "content": "<b>Licence de Sociologie, étude des sciences humaines et sociales</b>\n\nAprès l'obtention de mon BAC, je souhaitais initialement rejoindre une école d'informatique. Cependant cela n'a pas été possible du fait de mon Bac Economique et Social. Je me suis alors dirigé vers une Faculté de Sociologie qui était mon second choix afin d'assouvir ma curiosité envers l'être humain au sein des sciences sociales.\n\nCette licence m'a permis d'obtenir un point de vue critique de notre société et de mieux comprendre les comportements tant individuels que collectifs.\n\nJ'ai également eu l'occasion de travailler et de développer ma sociabilité et mon sens des responsabilités au sein de de l'association des étudiants de sociologie de l'université.\n\nJ'ai pu notamment organiser des concerts, projections-débats, évènements culturels, etc."
    }
  ]
};

const profiles = {
  "elements": [
    {
      "title": "A propos de moi",
      "content": "Bonjour et bienvenue sur mon CV en ligne.\n\nJe m'apelle Lucas SOUMAILLE et je travaille actuellement chez Thales pour l'ingénierie logicielle.\nN'hésitez pas à parcourir le CV en naviguant via le menu sur la gauche et à en apprendre plus sur moi plus bas sur cette page !\nVous pouvez également me contacter et m'envoyer un mail directement ou télécharger mon CV au format PDF \n <b><u>CV PDF pas à jour.</u></b>\n\nBonne visite."
    },
    {
      "title": "Ma philosophie",
      "content": "<p style='line-height:2.5em;margin-bottom:0px;'><span class='fas fa-quote-left'></span>Il n'existe pas de sujet peu intéressant, il n'y a que des personnes peu intéressées.<span class='fas fa-quote-right'></span><p style='margin-left:140px;font-size:0.8em;font-style:italic;'>Gilbert Keith Chesterton</p></p><ul><li>Curieux</li><li>Sociable</li><li>Communiquant</li></ul>"
    },
    {
      "title": "Mes centres d'intérêts et hobbies",
      "content": "Je suis passionné par les nouvelles technologies et ce qui a deux roues et plus de 100ch !\n\n <ul><li>Moto</li><li>Simulation aérienne</li><li>DIY technologie (imprimante 3D, objets connectés, serveur local, etc.)</li><li>Développement mobile</li><li>Lecture de romans et mangas</li><li>Jeux vidéos</li><li>Vulgarisation scientifique</li><li>Bricolage</li></ul>"
    }
  ]
};

const skills = {
  "topics": [
    {
      "title": "Programmation",
      "skills": [
        {
          "label": "Python",
          "value": "80"
        },
        {
          "label": "Javascript",
          "value": "60"
        },
        {
          "label": "Groovy",
          "value": "60"
        },
        {
          "label": "Java",
          "value": "60"
        },
        {
          "label": "HTML5",
          "value": "50"
        },
        {
          "label": "Ionic Framework",
          "value": "50"
        },
        {
          "label": "Android Studio",
          "value": "30"
        },
        {
          "label": "Angular",
          "value": "30"
        },
        {
          "label": "Kafka",
          "value": "30"
        },
        {
          "label": "Talend",
          "value": "30"
        },
        {
          "label": "PHP",
          "value": "20"
        },
        {
          "label": "Bonita",
          "value": "15"
        },
        {
          "label": "JEE",
          "value": "10"
        }
      ]
    },
    {
      "title": "Déploiement et administration système",
      "skills": [
        {
          "label": "Linux Debian/RHEL distributions",
          "value": "80"
        },
        {
          "label": "Git",
          "value": "80"
        },
        {
          "label": "Docker",
          "value": "70"
        },
        {
          "label": "Ansible",
          "value": "70"
        },
        {
          "label": "Jenkins",
          "value": "70"
        },
        {
          "label": "Sécurité (fail2ban, configs, chiffrement,...)",
          "value": "50"
        },
        {
          "label": "Gitlab CI, Github workflow",
          "value": "50"
        }
      ]
    },
    {
      "title": "Base de données",
      "skills": [
        {
          "label": "PostgreSQL",
          "value": "80"
        },
        {
          "label": "MySQL",
          "value": "80"
        },
        {
          "label": "Oracle Database",
          "value": "80"
        },
        {
          "label": "MongoDb/NoSQL",
          "value": "60"
        }
      ]
    },
    {
      "title": "Méthodologie",
      "skills": [
        {
          "label": "Merise",
          "value": "90"
        },
        {
          "label": "UML",
          "value": "90"
        },
        {
          "label": "Maquettage",
          "value": "50"
        }
      ]
    }
  ]
}

const expPro = {
  "experiences": [
    {
      "title": "Maintenance Aéro",
      "img": "fa-fighter-jet",
      "date": "2020 - En cours",
      "role": "Tech Lead / Architecte logiciel",
      "technologies": ["./ressources/node.png"],
      "content": "Responsable technique du logiciel et de l'équipe d'ingénieurs logiciels (équipe de 10 personnes). \nEvolution de l'architecture applicative, analyse et rédaction des spécifications et des évolutions de fonctionnalités, ajout des fonctionnalités. \nMise en place cycle Agile, Scrum, Kanban. \nRevue des MR/PR, gestion de la dette technique."
    },
    {
      "title": "Multisectorielle",
      "img": "fa-chart-line",
      "date": "2020 - 2023",
      "role": "Tech Lead / Architecte logiciel",
      "technologies": ["./ressources/node.png"],
      "content": "Conception et réalisation d'une architecture applicative (auto-éditée) de dashboarding à destination du CCM basée sur des technologies opensources et libres de droit dans un contexte agnostique.\n\nL'ensemble est piloté par des scripts de déploiement Ansible et au travers de conteneurs (dockers). Cette application rassemble :\n- Scrapping de logs (sous différents formats)\n- Automatisation de process\n- Alerting\n- Monitoring"
    },
    {
      "title": "Aéronautique",
      "img": "fa-fighter-jet",
      "date": "2018 - 2021",
      "role": "Tech Lead / Architecte logiciel",
      "technologies": ["./ressources/node.png", "./ressources/html5-logo-31816.png", "./ressources/quadient.png"],
      "content": "Premier intervenant sur un projet destiné à être une solution auto-éditée par CGI qui aura compté jusq'à une dizaine de membres, j'ai pu participer à tous les aspects de la mise en place de la solution.\n\nPour une entreprise d'aéronautique française dans un premier temps, la solution doit permettre l'élaboration d'écrans de maintenances via une inteface facile et intuitive à destination du métier.\n\nLa solution évoluera ensuite pour embarquer un mode déconnecté, permettant de réaliser des opérations de maintenance sans connexion directe au serveur de l'entreprise ou à internet via un système de synchronisation.\n\nAu cours de ce projet j'ai ainsi réalisé les premières versions de démonstration, puis par la suite j'ai pris une part active aux décisions d'architectures (choix technologies, architecture package applicatif, serveur middleware centralisé, etc.), réalisé le suivi des développements, analyse et rédaction du besoin, suivi du processus UX/UI, présentation au DSI, ... "
    },
    {
      "title": "Industrie",
      "img": "fa-industry",
      "date": "2020 - 2023",
      "role": "Tech Lead / Consultant solution",
      "technologies": ["./ressources/node.png"],
      "content": "Au sein d'une multinationale française dans le secteur de l'industrie et du retail, j'apporte mon expertise et mon savoir au quotidien dans divers domaines (CCM, architecture, bonnes pratiques, suivi de production, déploiement, formations, entretiens d'embauche, etc.).\n\nMes rôles sont très divers : architecture technique et recommandations, expertise logicielle, analyse fonctionnelle lors de la mise en place de nouveaux processus métiers, développements, formation, suivi et accompagnement des nouveaux arrivants, etc.\n\nJ'échange et je suis en contact tous les jours avec le métier pour répondre aux mieux aux divers besoins et coordonner les développements entre les applications en amont et en aval.\n\nAu cours de ces réunions, j'échange en anglais, du fait des multiples nationalités des intervenants (espagnol, américain, français, etc.) et de mon équipe (à 95% indienne) que j'aide et que j'accompagne au sein d'un contexte Agile."
    },

    {
      "title": "Assurance",
      "img": "fa-house-damage",
      "date": "2019-2020",
      "role": "Expert technique / Analyste fonctionnel",
      "technologies": ["./ressources/node.png"],
      "content": "J'ai débuté ce projet en autonomie avec le client (une mutuelle française).\nMon rôle était la réalisation de spécifications afin de réaliser des templates de courriers et de l'expertise socle et développement sur de la conception documentaire sur les outils Quadient.\n\nJ'ai ainsi pu sécuriser ce client pour notre pôle et intégrer et former un collègue en vue de mon remplacement pour pouvoir me libérer pour d'autres activités."
    },
    {
      "title": "Finance",
      "img": "fa-euro-sign",
      "date": "2018-2020",
      "role": "Concepteur développeur",
      "technologies": ["./ressources/node.png"],
      "content": "Pour plusieurs grandes banques françaises, j'ai participé au développement applicatif du socle et des templates documentaires dans la chaîne CCM/éditique ainsi qu'au support niveau 3 pour la correction des bugs."
    }
  ]
}

const project = {
  "projets": [
    {
      "idCarousel": "ovh",
      "title": "Serveur OVH",
      "git": "Projet privé",
      "images": [],
      "periode": "2022 - En cours",
      "content": "Migration de la solution de \"Serveur à domicile\" que j'ai développé (voir plus bas) vers un VPS OVH, déployé avec Docker et Ansible.",
      "technos": ["Docker", "Ansible", "Shell script", "Linux", "Node JS", "PostgreSQL", "OVH VPS", "fail2ban", "Python"]
    },
    {
      "idCarousel": "cours",
      "title": "Cours de soutien",
      "git": "Projet public",
      "images": [
        {
          "link": "./ressources/ex_mastermind.PNG",
          "legend": "Exemple d'exercice"
        }
      ],
      "periode": "2020 - En cours",
      "content": "Cours de soutien en Python, Java et SQL donnés à un étudiant en prépa et école d'ingénierie dont certaines des ressources et exercices sont disponible sur <a target=\"_blank\" href=\"https://gitlab.com/LucasSoumaille/cours-python\">mon gitlab</a>.",
      "technos": ["Python", "Java", "SQL"]
    },
    {
      "idCarousel": "imgCompress",
      "title": "Logiciel de compression d'images",
      "git": "Projet public",
      "images": [
        {
          "link": "./ressources/compress.PNG",
          "legend": "GUI"
        }
      ],
      "periode": "2022",
      "content": "Création d'un logiciel de compression d'images réalisé en Python afin de pouvoir compresser les images présentes dans un dossier avec divers paramètres (ratio, qualité, taille, etc.).\nProjet réalisé via des librairies Python et intégrant un GUI qui a été packagé dans un executable et qui est disponible sur <a target=\"_blank\" href=\"https://gitlab.com/LucasSoumaille/compress\">mon gitlab</a>.",
      "technos": ["Python"]
    },
    {
      "idCarousel": "homeAssist",
      "title": "Application assistant personnel domestique",
      "git": "Projet privé",
      "images": [
        {
          "link": "./ressources/home_assist_accueil.jpg",
          "legend": "Accueil"
        },
        {
          "link": "./ressources/home_assist_cook.jpg",
          "legend": "Onglet cuisine"
        },
        {
          "link": "./ressources/home_assist_devices.jpg",
          "legend": "Appareils connectés au serveur"
        },
        {
          "link": "./ressources/home_assist_logistic.jpg",
          "legend": "Onglet logistique"
        },
        {
          "link": "./ressources/home_assist_meteo_main.jpg",
          "legend": "Onglet météo"
        },
        {
          "link": "./ressources/home_assist_meteo_detail.jpg",
          "legend": "Détail de la météo"
        }
      ],
      "periode": "2021 - En cours",
      "content": "L'objectif de cette application (anonymisée pour le CV) est de réaliser un assistant personnel domestique connecté à un serveur local herbergé sur un Raspberry Pi.\n\nL'application contient tous les onglets nécessaires pour pouvoir organiser et garder en mémoire les bons petits plats à faire et à refaire, les objets dont on se sert peu qu'on aurait tendance à racheter inutilement où à perdre, la gestion des plantes (arrosage, coupe, récolte, etc.), les données météos dans une/des pièces données, les dépenses de loisir et activités, des rappels pour des tâches périodiques à effectuer. Dans une certaine mesure, des onglets sont disponibles pour réaliser un suivi technique du serveur via l'application mobile.\n\n L'application est réalisée via le framework Ionic, packagée grâce à Android Studio et déployée sur les téléphones du domicile avec un certificat auto-signé.\n\nElle ne contient aucune données et récupère l'intégralité du contenu via des API en place sur le serveur en wifi local.",
      "technos": ["Ionic Framework", "Angular", "HTML", "CSS", "Android Studio"]
    },
    {
      "idCarousel": "homeServ",
      "title": "Serveur à domicile",
      "git": "Projet privé",
      "images": [],
      "periode": "2021 - En cours",
      "content": "Serveur local herbergé sur un Raspberry Pi rassemblant : un Owncloud privée pour le stockage de ressources pouvant être redirigé sur un nom de domaine OVH et sécurisé via différents moyens (paramètrage Apache, fail2ban, etc.), un Node JS rassemblant les API pour l'aplication d'assistant personnel domestique et d'apareils connectés (envoi des températures heure par heure pour stockage en BDD, etc.), des scripts Python permettant l'éxécution de tâches automatiques (dictée des tâches du jour liées à l'API Google Agenda via haut-parleur), des scripts de sauvegardes hebdomadaires (HDD OS + HDD Backup).\n\nL'intégralité de ces programmes, sources et paramétrages (hormis la solution Owncloud en elle-même) a été réalisée par mes soins.",
      "technos": ["Raspberry Pi", "Shell script", "Linux", "Node JS", "PostgreSQL", "Owncloud", "Apache", "fail2ban", "Python"]
    },
    {
      "idCarousel": "sysCo",
      "title": "Systèmes connectés",
      "git": "Projet privé",
      "images": [],
      "periode": "2021 - En cours",
      "content": "Réalisation de capteurs afin de recueillir des données exploitables par le serveur à domicile (sonde de températures, capteurs de mouvement, caméras, etc.).\n\nChaque système a été réalisé sur une breadboard avec le logiciel Arduino IDE pour le développement puis soudé, mise en place avec alimentation secteur/USB ou sur batterie rechargeable par USB.",
      "technos": ["ESP82", "Arduino IDE", "Electronique", "Breadboard"]
    },
    {
      "idCarousel": "vehicule",
      "title": "Application gestion de véhicule",
      "git": "Projet privé",
      "images": [
        {
          "link": "./ressources/app_calc_accueil.jpg",
          "legend": "Accueil"
        },
        {
          "link": "./ressources/app_calc_pleins.jpg",
          "legend": "Liste des pleins effectués"
        },
        {
          "link": "./ressources/app_calc_graph.jpg",
          "legend": "Graphique de consommation"
        },
        {
          "link": "./ressources/app_calc_entretiens.jpg",
          "legend": "Entretiens du véhicule"
        }
      ],
      "periode": "2018 - 2019",
      "content": "Application (anonymisée également), réalisée au cours de l'année de mon Bac +3 afin de répondre à un besoin personnel : suivre la consommation de ma moto ne disposant pas de cette fonctionnalité initialement.\n\nL'application a été réalisé complètement réalisée via Android Studio en natif (Java) avec une base SQLite afin de stocker les données sur le portable.\nElle permet de renseigner un plein avec ses informations utiles afin de calculer une consommation \"instantannée\"(entre deux pleins), ainsi qu'une consommation moyenne sur la durée qu'on peut réinitialiser quand on le souhaite.\nDes graphiques sont présents pour aider à la visualisation ainsi qu'un suivi des entretiens. Ce suivi peut être programmé pour le véhicule et obtenir ainsi des rappels en notification à l'approche de la date ou du kilométrage défini.\n\nCette application a répondu à tous les besoins du Bac +3 lors de sa présentation pour la validation du titre, réalisation de cahier des charges, MCD, personas, maquettes UI, base de données, développement objet, etc.",
      "technos": ["Android Studio", "Java", "SQLite"]
    },
    {
      "idCarousel": "sysEx",
      "title": "Système expert",
      "git": "Projet public",
      "images": [
        {
          "link": "./ressources/algo_gen.png",
          "legend": ""
        }
      ],
      "periode": "2019",
      "content": "Mise en place d'algorithmes génétiques afin de résoudre la problématique du voyageur de commerce (itinéraire satisfaisant à trouver parmis 15 positions géographiques, soit environ 43.5 milliards de possibilités en le moins d'itérations possibles).\n\nLa génération aléatoire, les croisements statistiques et la mutation génétique permettent de trouver un chemin satisfaisant en quelques milliers d'itérations seulement.\n\nCe programme a été réalisé en Python lors de TP sur les systèmes complexes en informatique et m'a particulièrement intéressé et motivé, c'est pourquoi je le présente ici.\n\nL'image est un résultat (environ 3 100 km) obtenu en 5000 itérations avec ce programme (disponible sur <a target=\"_blank\" href=\"https://gitlab.com/LucasSoumaille/algo_genetique\">mon gitlab</a>).",
      "technos": ["Python", "Matplotlib"]
    }//,
    //{
    //  "idCarousel": "aiMinecraft",
    //  "title": "Intelligence Artificielle Minecraft",
    //  "images" : [],
    //  "periode": "2019",
    //  "content" : "",
    //  "technos" : ["techno1", "techno2"]
    //}
  ]
}


const dataChart1 = {
  labels: [
    'Technique',
    'Pédagogie',
    'Communication',
    'Business',
    'Management',
    'Méthodologie'
  ],
  datasets: [{
    label: 'Compétences professionnelles',
    data: [80, 90, 90, 30, 40, 90],
    fill: true,
    backgroundColor: 'rgba(74, 166, 199, 0.4)',
    borderColor: 'rgb(31, 102, 128)',
    pointBackgroundColor: 'rgb(31, 102, 128)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgb(31, 102, 128)'
  }]
};

const dataChart2 = {
  labels: [
    'Persévérance',
    'Curiosité',
    'Rigueur',
    'Autonomie',
    "Travail d'équipe",
    'Patience'
  ],
  datasets: [{
    label: 'Traits de caractères',
    data: [90, 95, 90, 90, 80, 70],
    fill: true,
    backgroundColor: 'rgba(141, 105, 236, 0.4)',
    borderColor: 'rgb(143, 102, 254)',
    pointBackgroundColor: 'rgb(143, 102, 254)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgb(143, 102, 254)'
  }]
};
